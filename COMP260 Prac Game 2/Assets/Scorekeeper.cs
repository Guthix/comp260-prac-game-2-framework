﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scorekeeper : MonoBehaviour {

	static private Scorekeeper instance;
	public int pointsPerGoal = 1;
	private int[] score = new int[3];
	public int pointsToWin = 5;

	public Text[] scoreText; 

	// Use this for initialization
	void Start () {
		if (instance == null) {
			// save this instance
			instance = this;
		} else {
			// more than one instance exists
			Debug.LogError(
				"More than one Scorekeeper exists in the scene.");
		}  

		//resets the scores to 0
		for (int i = 0; i < 2; i++) {
			score [i] = 0;
			scoreText [i].text = "0";
		}

	}
		
	// Update is called once per frame
	void Update () {
		
	}

	public void OnScoreGoal(int player) {
		score[player] += pointsPerGoal;
		scoreText [player].text = score [player].ToString ();
		if (score [1] == 5) {//player 1 (Blue) has 10 points
			scoreText [2].text = "Player 1 wins!";

			//resets both scores to 0
			score [0] = 0;
			scoreText [0].text = "0";
			score [1] = 0;
			scoreText [1].text = "0";


		}
		if (score [0] == 5) {//player 2 (Blue) has 10 points
			scoreText [2].text = "Player 2 wins!";

			//resets both scores to 0
			score [0] = 0;
			scoreText [0].text = "0";
			score [1] = 0;
			scoreText [1].text = "0";
		}

	}



	static public Scorekeeper Instance {//makes it read only
		get { return instance; }
	}



}
