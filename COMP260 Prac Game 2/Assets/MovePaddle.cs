﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Collections;

[RequireComponent(typeof(Rigidbody))]

public class MovePaddle : MonoBehaviour {

	private Rigidbody rigidbody;

	public float speed = 10f;

	public Vector3 test;

	private bool useMouse = false;


	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;

		test = new Vector3 (0.0f, 0.0f, 1.0f);


	}
	
	// Update is called once per frame
	void Update () {

	}



	void FixedUpdate () {

		if (useMouse) {
			//moving with the mouse
			Vector3 pos = GetMousePosition ();
			Vector3 dir = pos - rigidbody.position;
			Vector3 vel = dir.normalized * speed;

			// check is this speed is going to overshoot the target
			float move = speed * Time.fixedDeltaTime;
			float distToTarget = dir.magnitude;

			if (move > distToTarget) {
				// scale the velocity down appropriately
				vel = vel * distToTarget / move;
			}

			rigidbody.velocity = vel;
		}
		

		if (!useMouse) {//ie using key board



//			Vector3 test = (Input.GetAxis("Horizontal")*speed, 0, 0, Input.GetAxis("Vertical")*speed);

//			rigidbody.position.Set (0, 0, 0);


//			rigidbody.position.Set = (0, 0, Input.GetAxis("Vertical")*speed);

			//using Transform.position
//			transform.position = new Vector3(
//				((Input.GetAxis("Horizontal")/10)+transform.position.x), 0, ((Input.GetAxis("Vertical")/10)+transform.position.z));


			//using ridibody.position
//			rigidbody.position = new Vector3(
//				((Input.GetAxis("Horizontal")/10)+rigidbody.position.x), 0, ((Input.GetAxis("Vertical")/10)+rigidbody.position.z));

			//using rigidbody.velocity
			rigidbody.velocity = new Vector3 (Input.GetAxis("Horizontal")*speed, 0, Input.GetAxis("Vertical")*speed );

			//using AddForce
//			rigidbody.AddForce(Input.GetAxis("Horizontal")*speed, 0, 0);
//			rigidbody.AddForce(0, 0, Input.GetAxis("Vertical")*speed);

		}



	}





	private Vector3 GetMousePosition() {
		// create a ray from the camera 
		// passing through the mouse position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		// find out where the ray intersects the XZ plane
		Plane plane = new Plane(Vector3.up, Vector3.zero);
		float distance = 0;
		plane.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}

	void OnDrawGizmos() {
		// draw the mouse ray
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition();
		Gizmos.DrawLine(Camera.main.transform.position, pos);
	}


}