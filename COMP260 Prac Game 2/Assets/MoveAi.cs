﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAi : MonoBehaviour {


	public Transform startingPos;
	private Rigidbody rigidbody;

	private PuckControl puck;
	private Rigidbody puckRigidbody;

	// Use this for initialization
	void Start () {
		puck = FindObjectOfType<PuckControl> ();

		rigidbody = GetComponent<Rigidbody>();
		ResetPosition();

		puckRigidbody = puck.GetComponent<Rigidbody> ();

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate () {

		Vector3 pos = new Vector3 (rigidbody.position.x , rigidbody.position.y, puckRigidbody.position.z);

		rigidbody.position = pos;

	}

	public void ResetPosition() {
		// teleport to the starting position
		rigidbody.MovePosition(startingPos.position);
		// stop it from moving
		rigidbody.velocity = Vector3.zero;

	}

}
