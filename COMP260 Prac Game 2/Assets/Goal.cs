﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {

	public AudioClip scoreClip;
	private AudioSource audio;
	public MoveAi ai;
	public int player; //who gets points for scoring the goal
	private Scorekeeper scoreKeeper;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
		ai = FindObjectOfType<MoveAi> ();
		scoreKeeper = FindObjectOfType<Scorekeeper>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider collider) {
		// play score sound
		audio.PlayOneShot (scoreClip);



		if (this.gameObject.name == "RedGoal") {//something's broken here, it always adds to element 0, even if 0 isnt being called (even works when -1 is being called)
			player = 0;
			Debug.Log ("Blue scores!");
		}
		if (this.gameObject.name == "BlueGoal") {
			player = 1;
			Debug.Log ("Red scores!");
		}
			
		//tell the scorekeeper
		Scorekeeper.Instance.OnScoreGoal(player);


		// reset the puck to its starting position
		PuckControl puck =         
			collider.gameObject.GetComponent<PuckControl>();
		puck.ResetPosition();

		//reset the AiPaddle to the start
		ai.ResetPosition();


	}

}
